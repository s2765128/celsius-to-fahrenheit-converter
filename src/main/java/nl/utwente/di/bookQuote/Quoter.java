package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private HashMap <String, Double> bookPrice = new HashMap<>();
    public double getBookPrice (String isbn) {
        double celsius = Double.parseDouble(isbn);
        return (celsius*1.8) + 32;
    }
}
